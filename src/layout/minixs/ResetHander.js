import { onBeforeMount, onMounted, onBeforeUnmount, watch, computed } from 'vue';
import { useRoute } from 'vue-router';
import store from '@/store';

const { body } = document;
const WIDTH = 992;

export default function() {
    const route = useRoute();    

    watch(route, (newVal, oldVal) => {
        if (storeGetters.value.device === 'mobile' && storeGetters.value.sidebar.opened) {
            store.dispatch('app/closeSideBar', { withoutAnimation: false });
        }
    });

    onBeforeMount(() => {
        window.addEventListener('resize', $_resizeHander);
    });

    onBeforeUnmount(() => {
        window.removeEventListener('resize', $_resizeHander);
    });

    onMounted(() => {
        if ($_isMobile()) {
            store.dispatch('app/toggleDevice', 'mobile');
            store.dispatch('app/closeSideBar', { withoutAnimation: true });
        }
    });
    
    // 是否為平板大小
    const $_isMobile = () => {
        const rect = body.getBoundingClientRect();
        return rect.width - 1 < WIDTH;
    };
    
    const $_resizeHander = () => {
        if (!document.hidden) {
            const isMobile = $_isMobile();
            store.dispatch('app/toggleDevice', isMobile ? 'mobile' : 'desktop');

            if (isMobile) {
                store.dispatch('app/closeSideBar', { withoutAnimation: true });
            }
        }
    };

    const storeGetters = computed(() => {
        return {
            sidebar: store.getters.sidebar,
            device: store.getters.device
        };
    });
    
    return {
        // methods
        $_isMobile,
        $_resizeHander,
        // computed
        storeGetters
    };
}