const path = require('path');
const defaultSettings = require('./src/settings.js');
const { defineConfig } = require('@vue/cli-service');

const name = defaultSettings.title || 'vue base framework';

module.exports = defineConfig({
    publicPath: '/',
    outputDir: 'dist',
    assetsDir: 'static',
    productionSourceMap: false,
    lintOnSave: process.env.NODE_ENV === 'development',
    configureWebpack: {
        name: name,
        resolve: {
            alias: {
                '@': path.resolve(__dirname, 'src')
            }
        }
    }
});