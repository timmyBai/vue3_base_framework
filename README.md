# vue3 base framework

<div>
    <img src="https://img.shields.io/badge/vue-3.3.4-blue">
    <img src="https://img.shields.io/badge/vuex-4.1.0-blue">
    <img src="https://img.shields.io/badge/vue router-4.2.5-blue">
</div>

## 簡介

vue3 base framework 是一個幫助新手所建立的架構，它基於 vue 實建，它使用最新前端技術，提供完整的架構建置，可以幫助您快速建立企業級的架構，相信不管遇到什麼需求，這個架構可以幫助您，建置完美架構。

## 開發

```bash
# 克隆項目
git clone https://gitlab.com/timmyBai/vue3_base_framework.git

# 進入目錄
cd vue3_base_framework

# 安裝依賴
npm install
or
yarn install

# 啟用專案(development)
npm run serve:dev
or
yarn serve:dev

# 啟用專案(prodion)
npm run serve:dev
or
yarn serve:dev
```

## 打包

```bash
# development
npm run build:dev
或
yarn build

# production
npm run build:pro
或
yarn build:pro
```

## 打包 Docker

```docker
# development
docker-compose -f docker-compose.development.yml build && docker-compose -f docker-compose.development.yml up -d

# production
docker-compose -f docker-compose.production.yml build && docker-compose -f docker-compose.production.yml up -d
```
